import std.algorithm.iteration;
import std.algorithm.sorting;
import std.array;
import std.ascii;
import std.conv;
import std.exception;
import std.format;
import std.stdio;
import std.utf;

void main()
{
	string[int] texts;
	foreach (l; File("../xldlibs/english/systexts").byLine)
		if (l[0] == '[')
			texts[l[1..5].to!int] = l[6..$-2].idup;

	string toIdentifier(int num) { return format("ST_%04d_", num) ~ texts[num].byChar.map!(c => c.isAlphaNum ? c : '_').array.assumeUnique; }

	auto f = File("gensystexts-vars.idc", "w");
	enum base = 0x15D824;
	foreach (num; texts.byKey.array.sort)
	{
		auto addr = base + num * 4;
		f.writefln("create_dword(0x%x);", addr);
		f.writefln("set_name(0x%x, %(%s%));", addr, ["s" ~ toIdentifier(num)]);
	}

	f = File("gensystexts-enum.h", "w");
	f.writeln("enum SysTexts {");
	foreach (num; texts.byKey.array.sort)
		f.writefln("\t%s = %s,", toIdentifier(num), num);
	f.writeln("};");

	f = File("gensystexts-enum.idc", "w");
	f.writeln(`auto e = get_enum("SysTexts");`);
	foreach (num; texts.byKey.array.sort)
		f.writefln("add_enum_member(e, %(%s%), %d, -1);", [toIdentifier(num)], num);
}
