import ae.utils.meta;
import ae.utils.text;

import std.algorithm.iteration;
import std.array;
import std.ascii;
import std.exception;
import std.range;
import std.stdio;
import std.string;
import std.utf;

enum num = 49;
struct Data
{
	char[13][num] fileNames;
	char[32][num] fileDescs;
}

void main()
{
	auto f = File("MAIN.EXE");
	f.seek(0xFB1C5);
	Data data;
	f.rawRead((&data)[0..1]);

	auto o = File("descript.ion", "w");
	foreach (i; 0..num)
		o.writeln(data.fileNames[i].ptr.fromStringz, " ", data.fileDescs[i].ptr.fromStringz);

	o = File("xlds.h", "w");
	o.writeln("enum XLDs\n{");
	o.writefln("%-(\t%s,\n%)", num
		.iota
		.map!(i =>
			(data.fileDescs[i][0] ? data.fileDescs[i].ptr.fromStringz : data.fileNames[i].ptr.fromStringz.toLower)
			.byChar
			.map!(c => c.isAlphaNum ? c : ' ')
			.array
			.splitter
			.map!assumeUnique
			.array
			.camelCaseJoin
			.I!(s => s.length ? s : format("%d", i))
			.I!(s => "XLD_" ~ s)
		)
	);
	o.writeln("\n};");
}
