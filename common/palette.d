module albion.common.palette;

import std.file;

import ae.utils.graphics.color;

import albion.common.xld;

RGB[] getPalette(uint index)
{
	return cast(RGB[])(getXLDEntry!"palette"(index) ~ read(xldBase ~ "palette.000"));
}
