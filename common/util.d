module albion.common.util;

import std.exception;
import std.format;

ref T read(T)(ref void[] buf)
{
	enforce(buf.length >= T.sizeof, "Out of data while trying to read %s".format(T.stringof));
	T* t = cast(T*)buf.ptr;
	buf = buf[T.sizeof .. $];
	return *t;
}

T[] read(T)(ref void[] buf, size_t count)
{
	auto bytes = T.sizeof * count;
	enforce(buf.length >= bytes, "Out of data while trying to read array of %s (have %d of %d bytes)"
		.format(T.stringof, buf.length, bytes));
	T[] t = cast(T[])buf[0 .. bytes];
	buf = buf[bytes .. $];
	return t;
}
