module albion.common.map;

import std.bitmanip;
import std.exception;
import std.format;

import albion.common.types;
import albion.common.util;

struct Map
{
	struct Header
	{
		mixin(bitfields!(
			ushort, q{}                 , 13,
			bool  , q{packedMobs}       ,  1,
			bool  , q{moreMobs}         ,  1,
			ubyte , q{}                 ,  1,
		));

		enum Type : ubyte
		{
			none,
			t3D,
			t2D,
		}
		Type type;
		ubyte unknown3;
		ubyte width, height;
		ubyte iconSet;
		ubyte unknown7;
		ubyte palette;
		ubyte unknown;
	}
	static assert(Header.sizeof == 10);
	Header header;

	struct Mob
	{
		ubyte id;
		union
		{
			struct Packed
			{
				align(1):
				ubyte unknown1;
				ushort eventIndex;
				ushort npcID;
				mixin(bitfields!(
					Kind , q{kind}             , 3,
					ubyte, q{unknown2}         , 1,
					bool , q{hasTalkToMapText} , 1,
					ubyte, q{unknown10}        , 1,
					ubyte, q{visibilityTileBit}, 1,
					ubyte, q{}                 , 1,
				));
				ubyte pathKind;
			} Packed packed;

			struct Unpacked
			{
				align(1):
				ubyte visibilityTileBit;
				ushort eventIndex;
				ushort npcID;
				mixin(bitfields!(
					Kind , q{kind}             , 2,
					ubyte, q{pathKind}         , 2,
					bool , q{hasTalkToMapText} , 1,
					ubyte, q{}                 , 11,
				));
			} Unpacked unpacked;
		}
		TriggerMask eventTriggerMask;

		enum Kind
		{
			npc = 1,
			monsterGroup = 2,
		}

		enum PathKind
		{
			none = 0,
			looping = 4,
		}
	}
	static assert(Mob.sizeof == 10);
	Mob[] mobs;

	struct MobParser
	{
		Mob* mob;
		bool packed;

		@property ushort       eventIndex() const { return                    packed ? mob.packed.eventIndex : mob.unpacked.eventIndex ; }
		@property ushort       npcID     () const { return                    packed ? mob.packed.npcID      : mob.unpacked.npcID      ; }
		@property Mob.Kind     kind      () const { return cast(Mob.Kind    )(packed ? mob.packed.kind       : mob.unpacked.kind      ); }
		@property Mob.PathKind pathKind  () const { return cast(Mob.PathKind)(packed ? mob.packed.pathKind   : mob.unpacked.pathKind  ); }

		@property int pathLength() const
		{
			assert(mob.id);
			return (pathKind && pathKind != Mob.PathKind.looping) ? 1 : ticksPerHour * 24 /* hours per day */;
		}
	}

	struct Tile
	{
		ubyte b0, b1, b2;

		@property ushort overlay() { return (b0 << 4) | (b1 >> 4); }
		@property ushort underlay() { return ((b1 & 0xF) << 8) | b2; }
	}
	static assert(Tile.sizeof == 3);
	Tile[] tiles;

	struct Feature
	{
		ushort x;
		TriggerMask triggers;
		ushort blockIndex;
	}
	static assert(Feature.sizeof == 6);
	Feature[] mapFeatures;
	Feature[][] tileFeatures;

	struct EventBlock // Bar
	{
		EventType type;
		ubyte  data1;
		ubyte  data2;
		ubyte  data3;
		ubyte  data4;
		ubyte  data5;
		ushort data6;
		ushort data8;
		ushort next; // block index
	}
	static assert(EventBlock.sizeof == 12);
	EventBlock[] eventBlocks;

	struct MobPoint
	{
		ubyte x, y;
	}
	MobPoint[][] mobPaths;

	struct Quux
	{
		ubyte[19] unknown;
	}
	static assert(Quux.sizeof == 19);
	Quux[] quux;

	struct Xyzzy
	{
		ubyte[64] unknown;
	}
	Xyzzy xyzzy;

	ushort[] chains;

	this(void[] data)
	{
		header = data.read!Header;

		auto numMobs = header.moreMobs ? 0x60 : 0x20;
		mobs = data.read!Mob(numMobs);

		tiles = data.read!Tile(header.width * header.height);

		foreach (y; -1 .. header.height)
		{
			auto features = data.read!Feature(data.read!ushort);
			if (y == -1)
				mapFeatures = features;
			else
				tileFeatures ~= features;
		}

		if (!data.length)
			return; // TODO: find proper way to detect this

		eventBlocks = data.read!EventBlock(data.read!ushort);

		mobPaths.length = mobs.length;
		foreach (i, ref mob; mobs)
			if (mob.id)
				mobPaths[i] = data.read!MobPoint(MobParser(&mob, header.packedMobs).pathLength);

		if (header.type == Header.Type.t3D)
		{
			quux = data.read!Quux(data.read!ushort);
			xyzzy = data.read!Xyzzy;
		}

		if (!data.length)
			return; // TODO: find proper way to detect this

		chains = data.read!ushort(header.type == Header.Type.t3D ? 64 : 250);

		enforce(data.length == 0, "Stray data at end of file (%d bytes remaining)".format(data.length));
	}
}

struct IconData
{
	uint bits;
	ushort spriteIndex;
	ubyte unknown6;
	ubyte unknown7;
}
static assert(IconData.sizeof == 8);

immutable bool[] iconSetZoomedIn = [0, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1];
