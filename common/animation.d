module albion.common.animation;

import std.exception;
import std.format;

import ae.utils.graphics.image;

import albion.common.util;

struct AnimationHeader
{
	ushort width, height;
	ubyte unknown4;
	ubyte frames;
}

Image!ubyte[] parseAnimation(void[] data)
{
	auto header = data.read!AnimationHeader();
	auto result = new Image!ubyte[header.frames];
	foreach (ref frame; result)
	{
		frame.w = header.width;
		frame.h = header.height;
		frame.pixels = data.read!ubyte(header.width * header.height);
	}
	enforce(data.length == 0, "Stray data at end of file (%d bytes remaining)".format(data.length));
	return result;
}
