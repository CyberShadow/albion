module albion.common.xld;

import ae.utils.array;

import std.exception;
import std.file;
import std.format;

void[][] parseXLD(void[] bytes)
{
	enforce(bytes.length >= 8, "Too small for header");
	enforce(bytes[0..6] == "XLD0I\0", "Wrong header");
	auto numRecords = *cast(ushort*)(bytes.ptr + 6);
	bytes = bytes[8..$];
	enforce(bytes.length >= 4 * numRecords, "Too small for record list");
	auto lengths = cast(uint[])bytes[0 .. 4 * numRecords];
	bytes = bytes[4 * numRecords .. $];

	void[][] result;
	while (lengths.length)
	{
		enforce(bytes.length >= lengths[0], "Too small for record");
		result ~= bytes[0 .. lengths[0]];
		bytes = bytes[lengths[0] .. $];
		lengths = lengths[1 .. $];
	}
	enforce(bytes.length == 0, "Data past last record");
	return result;
	
}

enum xldBase = "../xldlibs/";

string getXLDFileName(string name, uint index)
{
	enforce(name.length <= 7, "Name too long!");
	return format("%s%s%d.xld", xldBase, name, index / 100);
}

private void[][] loadXLD(string s)
{
	static void[][][string] cache;
	auto pCache = s in cache;
	if (pCache) return *pCache;
	return cache[s] = read(s).parseXLD;
}

void[] getXLDEntry(string name)(uint index)
{
	static void[][] cache;
	void[] data = cache.get(index, null);
	if (data)
		return data;
	auto xld = getXLDFileName(name, index).loadXLD();
	auto subIndex = index >= 100 ? index % 100 : index - 1;
	enforce(subIndex < xld.length, "XLD index does not exist");
	return cache.putExpand(index, xld[subIndex]);
}
