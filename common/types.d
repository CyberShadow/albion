module albion.common.types;

enum Trigger
{
	normal,
	examine,
	touch,
	speak,
	useItem,
	mapInit,
	everyStep,
	everyHour,
	everyDay,
	default_,
	action,
	npc,
	take,
	script,
}

enum TriggerMask : ushort
{
	normal    = 1 << Trigger.normal   ,
	examine   = 1 << Trigger.examine  ,
	touch     = 1 << Trigger.touch    ,
	speak     = 1 << Trigger.speak    ,
	useItem   = 1 << Trigger.useItem  ,
	mapInit   = 1 << Trigger.mapInit  ,
	everyStep = 1 << Trigger.everyStep,
	everyHour = 1 << Trigger.everyHour,
	everyDay  = 1 << Trigger.everyDay ,
	default_  = 1 << Trigger.default_ ,
	action    = 1 << Trigger.action   ,
	npc       = 1 << Trigger.npc      ,
	take      = 1 << Trigger.take     ,
	script    = 1 << Trigger.script   ,
}

enum EventType : ubyte
{
	none,
	mapExit,
	door,
	chest,
	text,
	spinner,
	trap,
	changeUsedItem,
	datachange,
	changeIcon,
	encounter,
	placeAction,
	query,
	modify,
	action,
	signal,
	cloneAutomap,
	sound,
	startDialogue,
	createTransport,
	execute,
	removePartyMember,
	endDialogue,
	wipe,
	playAnimation,
	offset,
	pause,
	simpleChest,
	askSurrender,
	doScript,
}

enum ticksPerHour = 48;
