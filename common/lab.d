module albion.common.lab;

import std.exception;
import std.format;

import albion.common.types;
import albion.common.util;

struct Lab
{
	align(1)
	struct Header
	{
		uint   unknown0;
		ushort unknown4;
		ushort backgroundGraphicsIndex;
		uint   unknown8;
		uint   unknown12;
		uint   unknown16;
		uint   unknown20;
		uint   unknown24;
		uint   unknown28;
		uint   unknown32;
		ushort unknown36;
	}
	static assert(Header.sizeof == 0x26);
	Header header;

	struct Data66
	{
		ubyte[66] unknown;
	}
	static assert(Data66.sizeof == 66);
	Data66[] data66;

	align(1)
	struct Data10
	{
		uint unknown0;
		ubyte numFrames;
		ubyte unknown5;
		ushort floorIndex;
		ubyte unknown8;
		ubyte unknown9;
	}
	static assert(Data10.sizeof == 10);
	Data10[] data10;

	struct Data16
	{
		ubyte[16] unknown;
	}
	static assert(Data16.sizeof == 16);
	Data16[] data16;

	struct Data16B
	{
		uint   bits;
		ushort wallIndex;
		ubyte  numFrames;
		ubyte  autoMapTile;
		ushort unknown8;
		ushort width;
		ushort height;
		ushort numContents;

		struct Contents
		{
			ushort[6] unknown;
		}
		static assert(Contents.sizeof == 12);
	}
	static assert(Data16B.sizeof == 16);
	Data16B[] data16b;

	Data16B.Contents[][] data16bContents;

	this(void[] data)
	{
		header = data.read!Header;

		data66 = data.read!Data66(data.read!ushort);
		data10 = data.read!Data10(data.read!ushort);
		data16 = data.read!Data16(data.read!ushort);

		auto numData16B = data.read!ushort;
		foreach (n; 0..numData16B)
		{
			data16b ~= data.read!Data16B;
			data16bContents ~= data.read!(Data16B.Contents)(data16b[$-1].numContents);
		}

		enforce(data.length == 0, "Stray data at end of file (%d bytes remaining)".format(data.length));
	}
}
