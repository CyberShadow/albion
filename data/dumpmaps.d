module albion.data.dumpmaps;

import std.algorithm.iteration;
import std.base64;
import std.conv;
import std.exception;
import std.file;
import std.process;
import std.range;
import std.string;

import ae.sys.file;
import ae.utils.digest;
import ae.utils.graphics.color;
import ae.utils.graphics.image;
import ae.utils.xmlbuild;

import albion.common.animation;
import albion.common.map;
import albion.common.palette;
import albion.common.xld;
import albion.common.lab;

void dumpMap(uint index)
{
	auto map = Map(getXLDEntry!"mapdata"(index));

	auto iconSet = map.header.iconSet;
	enforce(iconSet > 0, "No iconset");
	
	enforce(map.header.palette > 0, "No palette");
	auto pal = getPalette(map.header.palette);

	if (map.header.type == Map.Header.Type.t2D)
	{
		auto iconData = cast(IconData[])getXLDEntry!"icondat"(iconSet);
		auto sprites = cast(ubyte[16][16][])getXLDEntry!"icongfx"(iconSet);

		auto fn = "maps/2d/%03d.png".format(index);
		if (!fn.exists)
		{
			auto i = Image!RGB(map.header.width * 16, map.header.height * 16);
			foreach (my; 0..map.header.height)
				foreach (mx; 0..map.header.width)
				{
					auto mapTile = map.tiles[my * map.header.width + mx];
					foreach (iconIndex; only(mapTile.underlay, mapTile.overlay))
						if (iconIndex >= 2)
						{
							auto spriteIndex = iconData[iconIndex - 2].spriteIndex;
							if (spriteIndex < sprites.length)
							{
								foreach (sy; 0..16)
									foreach (sx; 0..16)
									{
										auto b = sprites[spriteIndex][sy][sx];
										if (b)
											i[mx*16 + sx, my*16 + sy] = pal[b];
									}
							}
						}
				}
			ensurePathExists(fn);
			i.toPNG.toFile(fn);
		}

		fn = "maps/2d/%03d.svg".format(index);
		if (!fn.exists)
		{
			auto svg = newXml().svg();
			svg["xmlns"] = "http://www.w3.org/2000/svg";
			svg["version"] = "1.1";
			svg["xmlns:xlink"] = "http://www.w3.org/1999/xlink";
			svg["width"] = text(map.header.width * 16);
			svg["height"] = text(map.header.height * 16);
			auto svgDefs = svg.defs();

			auto wroteIcon = new bool[sprites.length];

			foreach (layer; 0..2)
			{
				auto svgG = svg.g();
				foreach (my; 0..map.header.height)
					foreach (mx; 0..map.header.width)
					{
						auto mapTile = map.tiles[my * map.header.width + mx];
						auto iconIndex = only(mapTile.underlay, mapTile.overlay)[layer];
						if (iconIndex >= 2)
						{
							auto spriteIndex = iconData[iconIndex - 2].spriteIndex;
							if (spriteIndex < sprites.length)
							{
								if (!wroteIcon[spriteIndex])
								{
									auto i = Image!RGBA(16, 16);
									foreach (y; 0..16)
										foreach (x; 0..16)
										{
											auto b = sprites[spriteIndex][y][x];
											if (b || layer == 0)
											{
												auto c = pal[b];
												i[x, y] = RGBA(c.r, c.g, c.b, 0xFF);
											}
										}

									auto svgImage = svgDefs.image();
									svgImage["id"] = format("%s%d", "uo"[layer], spriteIndex);
									svgImage["width"] = text(16);
									svgImage["height"] = text(16);
									svgImage["xlink:href"] = "data:image/png;base64," ~ Base64.encode(i.toSmallPNG).assumeUnique;

									wroteIcon[spriteIndex] = true;
								}

								auto svgUse = svgG.use();
								svgUse["x"] = text(mx * 16);
								svgUse["y"] = text(my * 16);
								svgUse["xlink:href"] = format("#%s%d", "uo"[layer], spriteIndex);
							}
						}
					}
			}

			bool zoomedIn = iconSetZoomedIn[map.header.iconSet - 1];

			{
				auto svgG = svg.g();
				uint[uint] npcHeight;
				foreach (mobIndex, ref mob; map.mobs)
					if (mob.id)
					{
						auto pmob = Map.MobParser(&mob, map.header.packedMobs);

						auto x = map.mobPaths[mobIndex][0].x;
						auto y = map.mobPaths[mobIndex][0].y;
						if (x == 0 || y == 0)
							continue;

						if (pmob.npcID !in npcHeight)
						{
							auto npcAnimData = (zoomedIn ? &getXLDEntry!"npcgr" : &getXLDEntry!"npckl")(pmob.npcID);
							auto npcAnim = parseAnimation(npcAnimData);
							auto i = npcAnim[7].colorMap!(b => b ? RGBA(pal[b].r, pal[b].g, pal[b].b, 0xFF) : RGBA.init).copy;

							auto svgImage = svgDefs.image();
							svgImage["id"] = format("n%d", pmob.npcID);
							svgImage["width"] = text(i.w);
							svgImage["height"] = text(i.h);
							svgImage["xlink:href"] = "data:image/png;base64," ~ Base64.encode(i.toSmallPNG).assumeUnique;

							npcHeight[pmob.npcID] = i.h;
						}

						auto svgUse = svgG.use();
						svgUse["x"] = text((x-1) * 16);
						svgUse["y"] = text((y  ) * 16 - npcHeight[pmob.npcID]);
						svgUse["xlink:href"] = format("#n%d", pmob.npcID);
					}
			}

			ensurePathExists(fn);
			svg.toString().toFile(fn);
		}
	}
	else
	{
		auto fn = "maps/3d-txt/%03d.png".format(index);
		if (!fn.exists)
		{
			auto lab = Lab(getXLDEntry!"labdata"(map.header.iconSet));

			auto i = Image!RGB(map.header.width * 64, map.header.height * 64);
			void[][uint] floors;

			foreach (my; 0..map.header.height)
				foreach (mx; 0..map.header.width)
				{
					auto mapTile = map.tiles[my * map.header.width + mx];
					auto idx = mapTile.b1;
					if (idx > 0 && idx <= lab.data10.length)
					{
						auto floorIndex = lab.data10[idx-1].floorIndex;
						auto floor = cast(ubyte[64][64][])getXLDEntry!"3dfloor"(floorIndex);
						if (floor.length)
							foreach (sy; 0..64)
								foreach (sx; 0..64)
								{
									auto b = floor[0][sy][sx];
									if (b)
										i[mx*64 + sx, my*64 + sy] = pal[b];
								}
						continue;
					}

					idx = mapTile.b0;
					if (idx >= 101 && (idx-101) < lab.data16b.length)
					{
						auto def = lab.data16b[idx-101];
						auto wallIndex = def.wallIndex;
						auto wall = cast(ubyte[])getXLDEntry!"3dwalls"(wallIndex);
						if (wall.length >= def.width * def.height)
						{
							auto wallI = ImageRef!ubyte(def.height, def.width, def.height, wall.ptr).flipXY.nearestNeighbor(64, 64);
							foreach (sy; 0..64)
								foreach (sx; 0..64)
								{
									auto b = wallI[sx, sy];
									if (b)
										i[mx*64 + sx, my*64 + sy] = pal[b];
								}
						}
					}
				}
			ensurePathExists(fn);
			i.toPNG.toFile(fn);
		}
	}
}

ubyte[] toSmallPNG(Image!RGBA image)
{
	auto base = "pngs/" ~ getDigestString!MD5(image.pixels).toLower;
	// pngout requires a .png output filename, otherwise it will unconditionally add one
	auto ifn = base ~ "-i.png";
	auto ofn = base ~ "-o.png";
	auto tfn = base ~ "-t.png";
	if (!ofn.exists)
	{
		ensurePathExists(ifn);
		image.toPNG.toFile(ifn);
		scope(exit) ifn.remove();
		scope(exit) if (tfn.exists) tfn.remove();
		spawnProcess(["pngout", "-y", "-c3", ifn, tfn]).wait();
		rename(tfn, ofn);
	}
	return cast(ubyte[])read(ofn);
}

void main(string[] args)
{
	import std.stdio;
	auto maps = args.length > 1 ? args[1..$].map!(to!int).array : iota(100, 400).array;
	foreach (n; maps)
	{
		writef("%d: ", n);
		try
		{
			dumpMap(n);
			writeln("OK!");
		}
		catch (Exception e)
			writeln(e.msg);
	}
}
