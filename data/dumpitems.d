import std.algorithm.comparison;
import std.algorithm.iteration;
import std.algorithm.mutation;
import std.algorithm.searching;
import std.algorithm.sorting;
import std.conv;
import std.exception;
import std.file;
import std.range;
import std.stdio;
import std.string;
import std.traits;
import std.typecons;

import ae.utils.array;

struct ItemName
{
	char[20] german, english, french;
}

C[] fromBuf(C)(C[] chars)
{
	auto p = chars.indexOf(0);
	return chars[0.. p < 0 ? $ : p];
}

string intStr(T)(T v)
{
	return format(format("%%%ds", 1 + 2 * v.sizeof), v);
}

string enumStr(E)(E e)
{
	enum id = __traits(identifier, E);
	enum namesId = toLower(id[0..1]) ~ id[1..$] ~ "Names";
	static if (is(typeof(mixin(namesId))))
	{
		mixin("alias names = " ~ namesId ~ ";");
		auto name = names.get(e, null);
		if (name)
			return name;
		else
			return text(cast(OriginalType!E)e);
	}
	else
	{
		auto s = text(e);
		if (s.startsWith("cast("))
			s = s.findSplit(")")[2];
		return s;
	}
}

string alignedEnumStr(E)(E e)
{
	enum maxLength = (){
		size_t maxLength = 0;
		foreach (e; EnumMembers!E)
			maxLength = max(maxLength, enumStr(e).length);
		return maxLength;
	}();
	return format(format("%%-%ds", max(maxLength, 1 + 2 * E.sizeof)), enumStr(e));
}

struct Unknown(T) { T value; }
struct Unused(T) { T value; }

enum ItemType : ubyte
{
	helmet = 2,
	shield = 4,
	closeRangeWeapon = 5,
	longRangeWeapon = 6,
	ammo = 7,
}

immutable string[] itemTypeNames = [
	null,
	"Armor",
	"Helmet",
	"Shoes",
	"Shield",

	"Close-range weapon",
	"Long-range weapon",
	"Ammo",
	"Document",
	"Spell scroll",

	"Drink",
	"Amulet",
	"Breast pin",
	"Ring",
	"Valuable",

	"Tool",
	"Key",
	"Normal",
	"Magical item",
	"Special item",

	"Transportation",
	"Lockpick",
	"Torch",
];

enum ItemEquipSlot : ubyte
{
	none = 0,
	neck = 1,
	head = 2,
	rightHand = 4,
	torso = 5,
	leftHand = 6,
	ringRight = 7,
	feet = 8,
	ringLeft = 9,
	rightHandAndTail = 10,
}

struct Hands { ubyte value; string toString() { return ["-", "1-hand", "2-hand"][value].format!"%-6s"; }}
enum AmmoClass : ubyte { none, arrows, bolts, firearms }
struct Weight { ushort grams; string toString() { return format("%5dg", grams); }}
struct Price { ushort centigold; string toString() { return centigold ? format("%6.1f", centigold / 10.0) : "     -"; }}
//struct ClassMask { uint mask; string toString() { return format("%032b", mask); }}
struct ClassMask { uint mask; string toString() { return 10.iota.map!(b => (mask & (1<<b)) ? "PSIJDET??H"[b] : '-').array; }}

enum Attribute : ubyte
{
	strength,
	intelligence,
	dexterity,
	speed,
	stamina,
	luck,
	magicResistance,
	magicTalent,
}
immutable string[] attributeNames = ["STR", "INT", "DEX", "SPD", "STA", "LUC", "M-R", "M-T"];

enum Skill : ubyte
{
	closeRangeCombat,
	longRangeCombat,
	criticalHit,
	lockpicking,
}
immutable string[] skillNames = ["CLO", "LON", "CRI", "L-P"];

enum ClassMaskBits : uint
{
	nobody        = 0,
	pilot         = 1 << 0,
	scientist     = 1 << 1,
	iskaiWarrior  = 1 << 2,
	djiKasMage    = 1 << 3,
	druid         = 1 << 4,
	enlightened   = 1 << 5,
	technician    = 1 << 6,
//	oquloKambulos = 1 << 7,
	humanWarrion  = 1 << 9,
}

struct Bonus(E, size_t count = 1, bool negative = false, A = byte)
{
	E[count] type;
	A[count] amount;
	string toString()
	{
		string[count] results;
		foreach (n; 0..count)
			results[n] = (type[n] || amount[n]) ? format("%s %s", intStr(cast(A)(amount[n] * [1, -1][negative])), enumStr(type[n])) : "      -";
		return results[].join(" ");
	}
}

struct MagicSpell
{
	MagicClass magicClass;
	ubyte spell;
	//string toString() { return "%02X %02X".format(magicClass, spell); }
	string toString() { return "[%-7s] %-20s".format(spell ? enumStr(magicClass) : "-", spell ? spellNames[magicClass][spell] : "-"); }
}

enum MagicClass : ubyte
{
	djiKas,
	unknown1,
	druid,
	unknown2,
}
immutable string[] magicClassNames = ["Dji-Kas", "E. One", "Druid", "Oqulo"];

immutable string[][] spellNames =
[
	[
		null,
		"Thorn snare",
		"DELETED",
		"DELETED",
		"Hurry",
		"View of life",
		"Frost splinter",
		"Frost crystal",
		"Frost avalanche",
		"Light healing",
		"Blinding spark",
		"Blinding ray",
		"Blinding storm",
		"Sleep spores",
		"Thorn trap",
		"Remove trap",
		"DELETED",
		"Heal intoxication",
		"Heal blindness",
		"Heal poisoning",
		"Fungification",
		"Light",
	],
	[
		null,
		"Regeneration",
		"Map view",
		"Lifebringer",
		"Teleporter",
		"Healing",
		"Quick withdrawal",
		"DELETED",
		"DELETED",
		"Goddess's Wrath",
		"Irritation",
		"Recuperation",
	],
	[
		null,
		"Berserk",
		"Banish demon",
		"Banish demons",
		"Demon exodus",
		"Small fireball",
		"Magic shield",
		"Healing",
		"Boasting",
		"Shock",
		"Panic",
		"DELETED",
	],
	[
		null,
		"Fireball",
		"Lightning strike",
		"Fire rain",
		"Thunderbolt",
		"Fire hail",
		"Thunderstorm",
		"Lightning trap",
		"Big lightning trap",
		"Lightning mine",
		"Big lightning mine",
		"Steal life",
		"Steal magic",
		"Personal protection",
		"Kamulos's Gaze",
		"Remove trap",
	],
	[
	],
	[
		null,
		"Panic",
		"Poison breeze",
		"Irritation",
		"Plague breeze",
	]
];

struct ItemData
{
	Unused!ubyte unused0;
	ItemType itemType;
	ItemEquipSlot equipSlot;
	ubyte fragility; /// in 0.1%
	Unused!ubyte unused4 = Unused!ubyte(3);
	Hands hands;
	ubyte bonusLP;
	ubyte bonusSP; // or malus, when??
	Bonus!Attribute attributeBonus;
	Bonus!Skill skillBonus;
	ubyte protection;
	ubyte damage;
	AmmoClass ammoClass;
	Bonus!(Skill, 2, true) skillTax;
	Unknown!ubyte unknown19; // amount of light?
	Unknown!ubyte unknown20; // something related to long-range? probably projectile animation
	MagicSpell magicSpell;
	ubyte magicUses;
	ubyte enchantmentsCur, enchantmentsMax;
	Unknown!ubyte unknown26; // something related to magic items
	Unknown!ubyte unknown27; // internal class?
	Unknown!ubyte unknown28; // 4=cursed
	Unknown!ubyte unknown29; // inventory icon animation? (laser | light staff | blue staff, SEED | Triifalai seed | torch (burning)
	Weight weight;
	Price price;
	ushort spriteIndex;
	ClassMask classMask;

	string toString()
	{
		string[] fields;
		foreach (i, f; this.tupleof)
		{
			//enum name = __traits(identifier, this.tupleof[i]);
			static if (is(typeof(f) == Unknown!T, T))
				fields ~= format(format("%%0%dX", T.sizeof * 2), f.value);
			else
			static if (is(typeof(f) == Unused!ubyte))
				assert(f == typeof(this).init.tupleof[i]);
			else
			static if (is(typeof(f) == enum))
				fields ~= alignedEnumStr(f);
			else
			static if (is(typeof(&f.toString)))
				fields ~= text(f);
			else
			static if (is(typeof(f) : long))
				fields ~= intStr(f);
			else
				static assert(false, "Unknown type: " ~ typeof(f).stringof);
		}
		return fields.join(" ");
	}
}
static assert(ItemData.sizeof == 40);

void main()
{
	auto names = cast(ItemName[])read("../xldlibs/itemname.dat");
	auto datas = cast(ItemData[])read("../xldlibs/itemlist.dat");
	enforce(names.length == datas.length);

	auto categorize(size_t n) { return datas[n].itemType; }
	auto cats = names.length
		.iota
		.map!(n => tuple(categorize(n), n))
		.array
		.sort!((a, b) => [a[0]] < [b[0]], SwapStrategy.stable)
		.groupBy
		.map!(tuples => tuple(tuples.front[0], tuples.map!(t => t[1]).array));
	foreach (t; cats)
	{
		foreach (n; t[1])
			writefln("%-20s : %s", names[n].english.fromBuf, datas[n]);
		writeln("--------------------");
	}
}

