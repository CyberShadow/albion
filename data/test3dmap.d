module albion.data.test3dmap;

import std.algorithm.iteration;
import std.exception;
import std.format;
import std.range;
import std.stdio;

import ae.utils.graphics.color;
import ae.utils.graphics.image;

import albion.common.map;
import albion.common.lab;
import albion.common.palette;
import albion.common.xld;

void checkMap(uint index)
{
	auto mapData = getXLDEntry!"mapdata"(index);
	enforce(mapData.length, "No map");
	auto map = Map(mapData);

	if (map.header.type != Map.Header.Type.t3D)
		throw new Exception("Not 3D");

	enforce(map.header.palette > 0, "No palette");
	auto pal = getPalette(map.header.palette);

	auto lab = Lab(getXLDEntry!"labdata"(map.header.iconSet));

	// wall/floor/ceiling?

	bool[4096] sawUnderlay, sawOverlay;
	bool[4096][3] sawB;

	foreach (my; 0..map.header.height)
		foreach (mx; 0..map.header.width)
		{
			auto mapTile = map.tiles[my * map.header.width + mx];
			sawUnderlay[mapTile.underlay] = true;
			sawOverlay[mapTile.overlay] = true;
			sawB[0][mapTile.b0] = true;
			sawB[1][mapTile.b1] = true;
			sawB[2][mapTile.b2] = true;
		}

	// 4096.iota.map!(n => cast(ubyte)((ubyte(sawUnderlay[n]) + ubyte(sawOverlay[n])) * 0x7F)).array.toFile("sawTiles/%04d".format(index));
	// foreach (b; 0..3)
	// 	4096.iota.map!(n => cast(ubyte)(sawB[b][n] * 0xFF)).array.toFile("sawTilesB/%d-%04d".format(b, index));

	void tryStructs(T)(T[] arr)
	{
		//writefln("%s: %d / %d/%d", T.stringof, arr.length, sawUnderlay.length, sawOverlay.length);
		// writefln("%s: %d / %d/%d/%d", T.stringof, arr.length, sawB[0].length, sawB[1].length, sawB[2].length);
	}

	tryStructs(lab.data66);
	tryStructs(lab.data10);
	tryStructs(lab.data16);
	tryStructs(lab.data16b);
	tryStructs(lab.data16bContents.join);

	writeln;
	foreach (ref ld16b; lab.data16b)
		with (ld16b)
			writefln("%(%4d\t%)", [unknown4, unknown6, autoMapTile, unknown8, unknown10, unknown12]);
}

void main()
{
	foreach (n; 100..400)
	{
		writef("%d: ", n);
		try
		{
			checkMap(n);
			writeln("OK!");
		}
		catch (Exception e)
			writeln(e.msg);
	}

	//writeln([minUnderlay, maxUnderlay, minOverlay, maxOverlay]);
	// foreach (n; 0..4096)
	// 	writefln("%4d %d %d", n, sawUnderlay[n], sawOverlay[n]);
}
