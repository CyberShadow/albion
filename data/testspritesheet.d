module albion.data.testspritesheet;

import std.exception;
import std.format;
import std.math;
import std.range;
import std.stdio;

import ae.utils.graphics.color;
import ae.utils.graphics.image;

import albion.common.map;
import albion.common.palette;
import albion.common.xld;
import albion.common.lab;

void main()
{
	enum index = 100;
	auto map = Map(getXLDEntry!"mapdata"(index));

	auto iconSet = map.header.iconSet;
	enforce(iconSet > 0, "No iconset");
	
	enforce(map.header.palette > 0, "No palette");
	auto pal = getPalette(map.header.palette);

	enforce(map.header.type == Map.Header.Type.t2D);

	auto iconData = cast(IconData[])getXLDEntry!"icondat"(iconSet);
	auto sprites = cast(ubyte[16][16][])getXLDEntry!"icongfx"(iconSet);

	auto sq = cast(int)sqrt(float(sprites.length));
	foreach (outputWidth; [1, 2, 4, 8, 16, sq / 4, sq / 2, sq, sq * 2, sq * 4, sprites.length / 4, sprites.length / 2, sprites.length])
	{
		writeln(outputWidth);
		auto i = Image!RGB(cast(int)(outputWidth * 16), cast(int)((sprites.length + outputWidth - 1) / outputWidth * 16));
		foreach (spriteIndex; 0..sprites.length)
		{
			auto mx = cast(int)(spriteIndex % outputWidth);
			auto my = cast(int)(spriteIndex / outputWidth);
			foreach (sy; 0..16)
				foreach (sx; 0..16)
				{
					auto b = sprites[spriteIndex][sy][sx];
					if (b)
						i[mx*16 + sx, my*16 + sy] = pal[b];
				}
		}
		i.toPNG.toFile("testspritesheet.out/%04d.png".format(outputWidth));
	}
}
