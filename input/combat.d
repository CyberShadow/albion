import core.time;

import std.algorithm.comparison;
import std.algorithm.searching;
import std.algorithm.sorting;
import std.digest.crc;
import std.exception;
import std.math;
import std.random;
import std.range;
import std.range.primitives;
import std.stdio;

import ae.utils.graphics.color;
import ae.utils.graphics.image;

import action;
import screen;

enum Character : uint
{
	empty    = 0xBAF465AE,
	Tom      = 0x1121EC27,
	Rainer   = 0x0AF887AD,
	Drirr    = 0x9D7B72D4,
	Sira     = 0xD7214BF6,
	Mellthas = 0x09D1D7D2,
	Siobhan  = 0x7EB80A85,
	Khunag   = 0x43E5E36C,
	Harriet  = 0xE795C4E6,
	// special
	damage   = 0x00000001,
	invalid  = 0x00000002,
}

immutable Character[] heroes = [
	Character.Tom,
	Character.Rainer,
	Character.Drirr,
	Character.Sira,
	Character.Mellthas,
	Character.Siobhan,
	Character.Khunag,
	Character.Harriet,
];

bool hasRanged(Character character) { return character == Character.Rainer || character == Character.Sira || character == Character.Harriet; }

enum BoardW = 6;
enum BoardH = 5;
enum BoardX0 = 82;
enum BoardY0 = 32;
enum BoardTW = 32;
enum BoardTH = 24;

immutable Image!RGB damageStripe;
shared static this() { damageStripe = loadSprite("battle-damage.bmp"); }

Character readCell(int cx, int cy)
{
	RGB[BoardTW] buf;
	auto screen = getScreen();

	if (spriteMatch(damageStripe, BoardX0 + BoardTW * cx, BoardY0 + BoardTH * cy, screen))
		return Character.damage;

	int grayPixels;
	foreach (x; only(1, BoardTW-2))
		foreach (y; 1..BoardTH-1)
		{
			auto p = screen[
				BoardX0 + BoardTW * cx + x,
				BoardY0 + BoardTH * cy + y,
			];
			auto minValue = min(p.r, p.g, p.b);
			auto maxValue = max(p.r, p.g, p.b);
			if (maxValue - minValue < 16)
			{
				auto medValue = (minValue + maxValue) / 2;
				if (medValue > 20 && medValue < 72)
					grayPixels++;
			}
		}
	if (grayPixels < 20) // 44 is max, 7 is typical for highlighted cell
		return Character.invalid;

	foreach (col; 0..BoardTW)
	{
		auto x = BoardX0 + BoardTW * cx + col;
		auto y = BoardY0 + BoardTH * cy + 0;
		buf[col] = screen[x, y];
	}

	static immutable BGs = [
		// 2 repeating columns across 4 repeating rows, in different palettes
		x"716D69716D69716D69716D69716D6965615D65615D716D69797975716D69716D69716D69797975797975797975716D69716D6965615D65615D65615D716D69716D69797975716D69716D69716D69716D69716D69716D69716D6965615D1C1810",
		x"65615D65615D797975868A8A797975716D69716D6965615D65615D716D69716D69797975797975797975716D6965615D716D69797975716D69716D69716D69716D69797975797975716D69716D69716D69716D69716D69797975716D69282824",
		x"797975797975716D69716D69716D69716D69797975797975797975797975797975797975797975797975797975716D69716D69716D6965615D716D69716D69716D6965615D65615D65615D716D69797975797975797975797975797975383838",
		x"797975716D69716D69797975797975716D69716D69716D69716D69797975797975797975868A8A797975797975797975716D69716D69716D6965615D716D69797975868A8A868A8A868A8A868A8A797975716D69716D6965615D65615D303030",
		x"716D69716D69716D69716D69716D6965615D65615D716D69797975797975868A8A797975797975797975797975716D69716D6965615D716D69797975797975797975716D69716D69716D69797975797975716D69716D69716D6965615D1C1810",
		x"716D69716D69716D69716D69716D6965615D65615D716D69797975797975868A8A797975797975716D69716D6965615D65615D65615D65615D65615D716D69797975716D69797975797975797975797975716D69716D69716D6965615D24201C",
		x"65615D65615D65615D65615D65615D797975716D69716D69716D69716D69716D69716D69716D69797975868A8A797975797975716D69716D6965615D65615D716D69716D69716D69716D69716D69716D69716D69716D69716D69716D69282824",
		x"716D69716D69716D69716D69797975716D69716D69716D69716D69716D69716D69716D69797975716D6965615D65615D65615D65615D716D69716D6965615D65615D716D69716D69716D69797975797975716D69716D69716D69716D69282824",
		/*
		x"716D69716D69716D69716D69716D695D59555D5955716D69797975716D69716D69716D69797975797975797975716D69716D6965615D5D595565615D716D69716D69797975716D69716D69716D69716D69716D69716D69716D6965615D1C1810",
		x"5D59555D5955797975868A8A797975716D69716D6965615D65615D716D69716D69797975797975797975716D6965615D716D69797975716D69716D69716D69716D69797975797975716D69716D69716D69716D69716D69797975716D69282824",
		x"797975797975716D69716D69716D69716D69797975797975797975797975797975797975797975797975797975716D69716D69716D695D5955716D69716D69716D6965615D5D595565615D716D69797975797975797975797975797975383838",
		x"797975716D69716D69797975797975716D69716D69716D69716D69797975797975797975868A8A797975797975797975716D69716D69716D6965615D716D69797975868A8A868A8A868A8A868A8A797975716D69716D6965615D65615D303030",
		x"716D69716D69716D69716D69716D695D595565615D716D69797975797975868A8A797975797975797975797975716D69716D6965615D716D69797975797975797975716D69716D69716D69797975797975716D69716D69716D695D59551C1810",
		x"716D69716D69716D69716D69716D695D595565615D716D69797975797975868A8A797975797975716D69716D6965615D65615D5D59555D595565615D716D69797975716D69797975797975797975797975716D69716D69716D6965615D24201C",
		x"5D59555D595565615D5D59555D5955797975716D69716D69716D69716D69716D69716D69716D69797975868A8A797975797975716D69716D6965615D65615D716D69716D69716D69716D69716D69716D69716D69716D69716D69716D69282824",
		x"716D69716D69716D69716D69797975716D69716D69716D69716D69716D69716D69716D69797975716D6965615D5D59555D59555D5955716D69716D6965615D65615D716D69716D69716D69797975797975716D69716D69716D69716D69282824",

		x"696969696969716D69716D696969695D59555D5955696969797975696969696969696969717971797975717971716D6969696965615D5D595565615D716D69716D69717971716D69696969696969696969716D69696969716D6965615D1C1810", 
		x"5D59555D5955717971868679868679716D6969696965615D65615D696969696969717971797975717971716D6965615D716D69717971716D69696969716D69716D69797975797975716D69696969696969696969716D69717971716D69282824", 
		x"717971717971716D69716D69696969716D69717971797975797975717971797975797975797975868679717971696969696969716D695D595569696969696969696965615D5D595565615D716D69717971797975797975797975797975383838", 
		x"717971716D69716D69717971717971716D69696969716D69716D69717971797975868679868679868679717971717971696969716D69716D6965615D6969697179718686798E86868E8686868679868679716D6969696965615D65615D303030", 
		x"696969696969696969716D69716D695D595565615D716D69797975868679868679868679797975797975797975716D6969696965615D716D69717971797975717971716D69716D69716D69717971717971716D696969696969695D59551C1810", 
		x"696969696969696969716D69716D695D595565615D716D69797975868679868679868679717971716D6969696965615D65615D5D59555D595565615D716D69717971716D69717971717971717971717971716D6969696969696965615D24201C", 
		x"5D59555D595565615D5D59555D5955717971696969716D69696969696969716D69696969696969717971868679717971797975716D6969696965615D65615D716D69696969696969696969716D69696969716D69716D69696969696969282824", 
		x"696969696969716D69716D69717971716D69716D69696969696969696969716D69716D69717971716D6965615D5D59555D59555D5955716D6969696965615D65615D716D69696969696969717971797975716D69696969696969696969282824",

		x"716D69716D69716D69716D69716D6965615D65615D716D69797975716D69716D69716D69797975797975797975716D69716D6965615D65615D65615D716D69716D69797975716D69716D69716D69716D69716D69716D69716D6965615D1C1810", 
		x"65615D65615D797975868A8A75797D716D69716D6965615D65615D716D69716D69797975797975797975716D6965615D716D69797975716D69716D69716D69716D69797975797975716D69716D69716D69716D69716D69797975716D69282824", 
		x"797975797975716D69716D69716D69716D6979797579797579797579797579797579797579797575797D797975716D69716D69716D6965615D716D69716D69716D6965615D65615D65615D716D69797975797975797975797975797975383838", 
		x"797975716D69716D69797975797975716D69716D69716D69716D6979797579797575797D868A8A75797D797975797975716D69716D69716D6965615D716D69797975868A8A868A8A8E9296868A8A75797D716D69716D6965615D65615D303030", 
		x"716D69716D69716D69716D69716D6965615D65615D716D6979797575797D868A8A75797D797975797975797975716D69716D6965615D716D69797975797975797975716D69716D69716D69797975797975716D69716D69716D6965615D1C1810", 
		x"716D69716D69716D69716D69716D6965615D65615D716D6979797575797D868A8A75797D797975716D69716D6965615D65615D65615D65615D65615D716D69797975716D69797975797975797975797975716D69716D69716D6965615D24201C", 
		x"65615D65615D65615D65615D65615D797975716D69716D69716D69716D69716D69716D69716D69797975868A8A797975797975716D69716D6965615D65615D716D69716D69716D69716D69716D69716D69716D69716D69716D69716D69282824", 
		x"716D69716D69716D69716D69797975716D69716D69716D69716D69716D69716D69716D69797975716D6965615D65615D65615D65615D716D69716D6965615D65615D716D69716D69716D69797975797975716D69716D69716D69716D69282824",
		*/
	];

	version(none) writefln("%-(%02X%)", cast(ubyte[])buf[]);

	enum threshold = 20;
	foreach (bg; cast(RGB[][])BGs)
		foreach (col, ref p; buf)
			if (abs(int(p.r) - int(bg[col].r)) < threshold &&
				abs(int(p.g) - int(bg[col].g)) < threshold &&
				abs(int(p.b) - int(bg[col].b)) < threshold)
				p = RGB.init;

	union U
	{
		ubyte[4] digest;
		Character character;
	}
	U u;
	u.digest = crc32Of(buf[]);
	return u.character;
}

enum maxPortraits = 6;

struct State
{
	Character[BoardW][BoardH] board;

	struct Portrait
	{
		Character character;

		enum maxPoints = 20;
		bool present;
		int hp; // out of 20
		bool hasMagic;
		int mp; // out of 20
	}
	Portrait[maxPortraits] portraits;
}

State.Portrait readPortrait(int index)
{
	auto screen = getScreen();

	State.Portrait result;
	auto px = 10 + index * 28;

	// Should be OCR'd, but hard-coded for now
	result.character = [Character.Tom, Character.Drirr, Character.Sira, Character.Mellthas, Character.Harriet, Character.Khunag][index];

	bool readBar(int py, out int points, RGB fg, RGB bg)
	{
		foreach (x; 0..State.Portrait.maxPoints)
		{
			auto c = screen[px+x, py];
			if (c != screen[px+x, py+1])
				return false;
			if (c == fg)
				continue;
			else
			if (c == bg)
			{
				points = x;
				return true;
			}
			else
				return false;
		}
		points = State.Portrait.maxPoints;
		return true;
	}

	result.present  = readBar(231, result.hp, RGB(0x75, 0x82, 0x2C), RGB(0x1C, 0x45, 0x24));
	result.hasMagic = readBar(234, result.mp, RGB(0x41, 0x8E, 0x8E), RGB(0x10, 0x41, 0x41));
	return result;
}

State readState()
{
	debug (save_board) getScreen().toPNG.toFile("board.png");
	State state;
	foreach (y; 0..BoardH)
		foreach (x; 0..BoardW)
			state.board[y][x] = readCell(x, y);
	foreach (i; 0..maxPortraits)
		state.portraits[i] = readPortrait(i);
	return state;
}

debug (bad_board)
{
	import ae.utils.graphics.im_convert;
	void main()
	{
		import std.file : read;
		"board-bad.png".read.parseViaIMConvert!BGR.colorMap!(c => RGB(c.r, c.g, c.b)).copy(screenBuffer);
		screenDirty = false;
		auto state = readState();
		writeln(state.board[4][0]);
	}
}

final class CombatAction : InputAction
{
	static __gshared bool[Character] needActionReset;

	protected override void impl()
	{
		//if (!active) return;

		mouseMove(screenW-1, screenH-1);

		auto state = readState();

		foreach (y; 0..BoardH)
			foreach (x; 0..BoardW)
				if (state.board[y][x] == Character.damage)
				{
					writefln("Board has damage balloon (at %d,%d)", x, y);
					clickCharacter(x, y);
					int mx, my;
					do
						mx = uniform(0, BoardW), my = uniform(0, BoardH);
					while (state.board[my][mx] == Character.empty);
					clickCharacter(mx, my);
					return;
				}
				else
				if (state.board[y][x] == Character.invalid)
				{
					writefln("Board invalid (at %d,%d)", x, y);
					mouseMove(screenW-2, screenH-2);
					return;
				}

		struct CharAction { int delegate() getScore; void delegate() execute; }
		CharAction[][maxPortraits] charActions;
		int[BoardW][BoardH] targeted;

		foreach (y; 0..BoardH)
			foreach (x; 0..BoardW)
				(x, y)
				{
					auto character = state.board[y][x];
					if (heroes.canFind(character))
					{
						writeln("Found ", character);

						auto portraitIndex = state.portraits[].countUntil!(p => p.character == character);
						auto portrait = state.portraits[portraitIndex];
						if (portrait.hasMagic && portrait.hp + portrait.mp > 10 && character != Character.Sira && character != Character.Khunag)
						{
							foreach (targetIndex; 0 .. maxPortraits)
								if (state.portraits[targetIndex].present && state.portraits[targetIndex].hp <= 10)
									(targetIndex) {
										charActions[portraitIndex] ~= CharAction(
											{
												if (state.portraits[targetIndex].hp > 10)
													return 0; // healed by someone else
												int score = 20_000;
												score -= 100 * (targetIndex == portraitIndex ? 0 : 1); // heal self first!
												score -=   1 * state.portraits[targetIndex].hp; // heal more wounded first
												return score;
											},
											{
												foreach (ty; 0..BoardH)
													foreach (tx; 0..BoardW)
														if (state.board[ty][tx] == state.portraits[targetIndex].character)
														{
															clickCharacter(x, y, Button.right);
															selectMenuItem(3); // Use magic
															switch (character)
															{
																case Character.Mellthas:
																	click(180, 114);
																	break;
																case Character.Harriet:
																	click(180, 94);
																	break;
																default:
																	assert(false);
															}
															while (findFrames().length)
																sleep(frameDuration);
															selectTarget(tx, ty);
															needActionReset[character] = true;
															state.portraits[targetIndex].hp += 5; // don't double-heal unless necessary
														}
											});
									} (targetIndex);
						}

						foreach (ey; 0..BoardH)
							foreach (ex; 0..BoardW)
								(ex, ey)
								{
									auto e = state.board[ey][ex];

									if (e == Character.empty)
										return;
									if (heroes.canFind(e))
										return;

									auto distance = max(abs(x-ex), abs(y-ey));
									if (!hasRanged(character) && distance > 1)
										return;

									bool engaged;
									foreach (sy; ey-1 .. ey+1 +1)
										foreach (sx; ex-1 .. ex+1 +1)
											if (sx >= 0 && sx < BoardW && sy >= 0 && sy < BoardH)
												if (heroes.canFind(state.board[sy][sx]))
													engaged = true;

									/*
									  - prepare engaged over non-engaged
									  - prepare enemies not attacked by others
									  - prepare closer (range-wise) rather than further
									  - prepare closer (to bottom of table) rather than further
									 */

									charActions[portraitIndex] ~= CharAction(
										{
											int score = 10000;
											score -= 1000 * (engaged ? 0 : 1);
											score -=  100 * targeted[ey][ex];
											score -=   10 * distance;
											score -=    1 * (BoardH - 1 - y);
											return score;
										},
										{
											clickCharacter(x, y, Button.right);
											selectMenuItem(1); // Attack
											selectTarget(ex, ey);
											needActionReset.remove(character);
											targeted[ey][ex]++;
										});
								}
								(ex, ey);

						if (character in needActionReset)
							charActions[portraitIndex] ~= CharAction(
								{ return 1; },
								{
									clickCharacter(x, y, Button.right);
									selectMenuItem(0); // Do nothing
									needActionReset.remove(character);
								});
					}
				} (x, y);

		auto order = maxPortraits.iota.array;
		order.sort!((a, b) => charActions[a].length < charActions[b].length);

		foreach (portraitIndex; order)
		{
			if (!state.portraits[portraitIndex].present)
				continue;

			if (!charActions[portraitIndex].length)
			{
				writefln("%s has nothing to do", state.portraits[portraitIndex].character);
				continue;
			}

			charActions[portraitIndex].sort!((a, b) => a.getScore() > b.getScore());
			auto bestAction = charActions[portraitIndex][0];
			auto bestScore = bestAction.getScore();
			if (bestScore <= 0)
			{
				writefln("%s no longer has anything good to do", state.portraits[portraitIndex].character);
				continue;
			}

			writefln("Executing %s's best action (%d points)", state.portraits[portraitIndex].character, bestScore);
			bestAction.execute();
		}

		mouseMove(178, 161);
		// if (!active) return;
		// sleep(1000.msecs); // confirm
		if (!active) return;
		click(178, 161);
	}

	private void clickCharacter(int x, int y, Button button = Button.left)
	{
		click(
			BoardX0 + BoardTW * x + BoardTW / 2,
			BoardY0 + BoardTH * y + BoardTH / 2,
			button
		);
	}

	private void selectTarget(int x, int y)
	{
		clickCharacter(x, y);
		while (!waitMouse(2, syncTimeout + doubleClickTimeout)) // golden
			clickCharacter(x, y);
	}

	private void selectMenuItem(int menuItem)
	{
		auto start = MonoTime.currTime;
	again:
		auto frames = findFrames();
		if (frames.length == 0)
		{
			auto elapsed = MonoTime.currTime - start;
			if (elapsed > syncTimeout * 2)
				throw new Exception("Timeout waiting for frame."); // we should be in a good state after that, so just retry
			else
				writefln("Waiting for frame (%s)...", elapsed);
			sleep(frameDuration);
			goto again;
		}
		enforce(frames.length == 1, "No / too many frames");

		click(frames[0][0] + 45, frames[0][1] + 28 + 12 * menuItem);

		auto frames2 = frames;
		while (frames == frames2)
		{
			writeln("Waiting for frame to be gone...");
			sleep(frameDuration);
			frames2 = findFrames();
		}
	}

	private bool waitMouse(int cursor, Duration timeout = Duration.max)
	{
		int mx, my;
		MonoTime start = MonoTime.currTime;
		while (findMouse(mx, my) != cursor)
		{
			if (MonoTime.currTime - start > timeout)
				return false;
			writeln("Waiting for cursor...");
			sleep(frameDuration);
		}
		return true;
	}
}

version(print_board)
void main()
{
	auto b = readState();
	foreach (y; 0..BoardH)
		writefln("%(%08X, %)", b.board[y][]);
}
