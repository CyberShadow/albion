/* SPDX-License-Identifier: GPL-2.0 WITH Linux-syscall-note */
/*
 * Copyright (c) 1999-2002 Vojtech Pavlik
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */
module linux.input;

import core.sys.linux.sys.time;
import core.sys.posix.sys.ioctl;

import linux.input_event_codes;

/*
 * The event structure itself
 * Note that __USE_TIME_BITS64 is defined by libc based on
 * application's request to use 64 bit time_t.
 */

struct input_event
{
//#if (__BITS_PER_LONG != 32 || !defined(__USE_TIME_BITS64)) && !defined(__KERNEL)
    timeval time;
// #define input_event_sec time.tv_sec
// #define input_event_usec time.tv_usec
// #else
//  __kernel_ulong_t __sec;
//  __kernel_ulong_t __usec;
// #define input_event_sec  __sec
// #define input_event_usec __usec
// #endif
    ushort type;
    ushort code;
    int value;
}

/*
 * Protocol version.
 */

enum EV_VERSION     = 0x010001;

/*
 * IOCTLs (0x00 - 0x7f)
 */

struct input_id
{
    ushort bustype;
    ushort vendor;
    ushort product;
    ushort version_;
}

/**
 * struct input_absinfo - used by EVIOCGABS/EVIOCSABS ioctls
 * @value: latest reported value for the axis.
 * @minimum: specifies minimum value for the axis.
 * @maximum: specifies maximum value for the axis.
 * @fuzz: specifies fuzz value that is used to filter noise from
 *  the event stream.
 * @flat: values that are within this value will be discarded by
 *  joydev interface and reported as 0 instead.
 * @resolution: specifies resolution for the values reported for
 *  the axis.
 *
 * Note that input core does not clamp reported values to the
 * [minimum, maximum] limits, such task is left to userspace.
 *
 * The default resolution for main axes (ABS_X, ABS_Y, ABS_Z)
 * is reported in units per millimeter (units/mm), resolution
 * for rotational axes (ABS_RX, ABS_RY, ABS_RZ) is reported
 * in units per radian.
 * When INPUT_PROP_ACCELEROMETER is set the resolution changes.
 * The main axes (ABS_X, ABS_Y, ABS_Z) are then reported in
 * in units per g (units/g) and in units per degree per second
 * (units/deg/s) for rotational axes (ABS_RX, ABS_RY, ABS_RZ).
 */
struct input_absinfo
{
    int value;
    int minimum;
    int maximum;
    int fuzz;
    int flat;
    int resolution;
}

/**
 * struct input_keymap_entry - used by EVIOCGKEYCODE/EVIOCSKEYCODE ioctls
 * @scancode: scancode represented in machine-endian form.
 * @len: length of the scancode that resides in @scancode buffer.
 * @index: index in the keymap, may be used instead of scancode
 * @flags: allows to specify how kernel should handle the request. For
 *  example, setting INPUT_KEYMAP_BY_INDEX flag indicates that kernel
 *  should perform lookup in keymap by @index instead of @scancode
 * @keycode: key code assigned to this scancode
 *
 * The structure is used to retrieve and modify keymap data. Users have
 * option of performing lookup either by @scancode itself or by @index
 * in keymap entry. EVIOCGKEYCODE will also return scancode or index
 * (depending on which element was used to perform lookup).
 */
struct input_keymap_entry
{
//enum INPUT_KEYMAP_BY_INDEX = (1 << 0);
    ubyte  flags;
    ubyte  len;
    ushort index;
    uint keycode;
    ubyte[32]  scancode;
}

struct input_mask
{
    uint type;
    uint codes_size;
    ulong codes_ptr;
}

enum EVIOCGVERSION       = _IOR!int('E', 0x01);            /* get driver version */
enum EVIOCGID            = _IOR!input_id('E', 0x02);    /* get device ID */
enum EVIOCGREP           = _IOR!(uint[2])('E', 0x03);    /* get repeat settings */
enum EVIOCSREP           = _IOW!(uint[2])('E', 0x03);    /* set repeat settings */

enum EVIOCGKEYCODE       = _IOR!(uint[2])('E', 0x04);        /* get keycode */
enum EVIOCGKEYCODE_V2    = _IOR!input_keymap_entry('E', 0x04);
enum EVIOCSKEYCODE       = _IOW!(uint[2])('E', 0x04);        /* set keycode */
enum EVIOCSKEYCODE_V2    = _IOW!input_keymap_entry('E', 0x04);

enum EVIOCGNAME(len)     = _IOC(_IOC_READ, 'E', 0x06, len);     /* get device name */
enum EVIOCGPHYS(len)     = _IOC(_IOC_READ, 'E', 0x07, len);     /* get physical location */
enum EVIOCGUNIQ(len)     = _IOC(_IOC_READ, 'E', 0x08, len);     /* get unique identifier */
enum EVIOCGPROP(len)     = _IOC(_IOC_READ, 'E', 0x09, len);     /* get device properties */

/**
 * EVIOCGMTSLOTS(len) - get MT slot values
 * @len: size of the data buffer in bytes
 *
 * The ioctl buffer argument should be binary equivalent to
 *
 * struct input_mt_request_layout {
 *  uint code;
 *  int values[num_slots];
 * };
 *
 * where num_slots is the (arbitrary) number of MT slots to extract.
 *
 * The ioctl size argument (len) is the size of the buffer, which
 * should satisfy len = (num_slots + 1) * sizeof(int).  If len is
 * too small to fit all available slots, the first num_slots are
 * returned.
 *
 * Before the call, code is set to the wanted ABS_MT event type. On
 * return, values[] is filled with the slot values for the specified
 * ABS_MT code.
 *
 * If the request code is not an ABS_MT value, -EINVAL is returned.
 */
enum EVIOCGMTSLOTS(len)  = _IOC(_IOC_READ, 'E', 0x0a, len);

enum EVIOCGKEY(len)      = _IOC(_IOC_READ, 'E', 0x18, len);     /* get global key state */
enum EVIOCGLED(len)      = _IOC(_IOC_READ, 'E', 0x19, len);     /* get all LEDs */
enum EVIOCGSND(len)      = _IOC(_IOC_READ, 'E', 0x1a, len);     /* get all sounds status */
enum EVIOCGSW(len)       = _IOC(_IOC_READ, 'E', 0x1b, len);     /* get all switch states */

enum EVIOCGBIT(ev,len)   = _IOC(_IOC_READ, 'E', 0x20 + (ev), len);  /* get event bits */
enum EVIOCGABS(abs)      = _IOR!input_absinfo('E', 0x40 + (abs));   /* get abs value/limits */
enum EVIOCSABS(abs)      = _IOW!input_absinfo('E', 0xc0 + (abs));   /* set abs value/limits */

enum EVIOCSFF            = _IOW!ff_effect('E', 0x80);   /* send a force effect to a force feedback device */
enum EVIOCRMFF           = _IOW!int('E', 0x81);            /* Erase a force effect */
enum EVIOCGEFFECTS       = _IOR!int('E', 0x84);            /* Report number of effects playable at the same time */

enum EVIOCGRAB           = _IOW!int('E', 0x90);            /* Grab/Release device */
enum EVIOCREVOKE         = _IOW!int('E', 0x91);            /* Revoke device access */

/**
 * EVIOCGMASK - Retrieve current event mask
 *
 * This ioctl allows user to retrieve the current event mask for specific
 * event type. The argument must be of type "struct input_mask" and
 * specifies the event type to query, the address of the receive buffer and
 * the size of the receive buffer.
 *
 * The event mask is a per-client mask that specifies which events are
 * forwarded to the client. Each event code is represented by a single bit
 * in the event mask. If the bit is set, the event is passed to the client
 * normally. Otherwise, the event is filtered and will never be queued on
 * the client's receive buffer.
 *
 * Event masks do not affect global state of the input device. They only
 * affect the file descriptor they are applied to.
 *
 * The default event mask for a client has all bits set, i.e. all events
 * are forwarded to the client. If the kernel is queried for an unknown
 * event type or if the receive buffer is larger than the number of
 * event codes known to the kernel, the kernel returns all zeroes for those
 * codes.
 *
 * At maximum, codes_size bytes are copied.
 *
 * This ioctl may fail with ENODEV in case the file is revoked, EFAULT
 * if the receive-buffer points to invalid memory, or EINVAL if the kernel
 * does not implement the ioctl.
 */
enum EVIOCGMASK      = _IOR!input_mask('E', 0x92);  /* Get event-masks */

/**
 * EVIOCSMASK - Set event mask
 *
 * This ioctl is the counterpart to EVIOCGMASK. Instead of receiving the
 * current event mask, this changes the client's event mask for a specific
 * type.  See EVIOCGMASK for a description of event-masks and the
 * argument-type.
 *
 * This ioctl provides full forward compatibility. If the passed event type
 * is unknown to the kernel, or if the number of event codes specified in
 * the mask is bigger than what is known to the kernel, the ioctl is still
 * accepted and applied. However, any unknown codes are left untouched and
 * stay cleared. That means, the kernel always filters unknown codes
 * regardless of what the client requests.  If the new mask doesn't cover
 * all known event-codes, all remaining codes are automatically cleared and
 * thus filtered.
 *
 * This ioctl may fail with ENODEV in case the file is revoked. EFAULT is
 * returned if the receive-buffer points to invalid memory. EINVAL is returned
 * if the kernel does not implement the ioctl.
 */
enum EVIOCSMASK      = _IOW!input_mask('E', 0x93);  /* Set event-masks */

enum EVIOCSCLOCKID   = _IOW!int('E', 0xa0);            /* Set clockid to be used for timestamps */

/*
 * IDs.
 */

enum ID_BUS          = 0;
enum ID_VENDOR       = 1;
enum ID_PRODUCT      = 2;
enum ID_VERSION      = 3;

enum BUS_PCI         = 0x01;
enum BUS_ISAPNP      = 0x02;
enum BUS_USB         = 0x03;
enum BUS_HIL         = 0x04;
enum BUS_BLUETOOTH   = 0x05;
enum BUS_VIRTUAL     = 0x06;

enum BUS_ISA         = 0x10;
enum BUS_I8042       = 0x11;
enum BUS_XTKBD       = 0x12;
enum BUS_RS232       = 0x13;
enum BUS_GAMEPORT    = 0x14;
enum BUS_PARPORT     = 0x15;
enum BUS_AMIGA       = 0x16;
enum BUS_ADB         = 0x17;
enum BUS_I2C         = 0x18;
enum BUS_HOST        = 0x19;
enum BUS_GSC         = 0x1A;
enum BUS_ATARI       = 0x1B;
enum BUS_SPI         = 0x1C;
enum BUS_RMI         = 0x1D;
enum BUS_CEC         = 0x1E;
enum BUS_INTEL_ISHTP = 0x1F;

/*
 * MT_TOOL types
 */
enum MT_TOOL_FINGER  = 0;
enum MT_TOOL_PEN     = 1;
enum MT_TOOL_PALM    = 2;
enum MT_TOOL_MAX     = 2;

/*
 * Values describing the status of a force-feedback effect
 */
enum FF_STATUS_STOPPED   = 0x00;
enum FF_STATUS_PLAYING   = 0x01;
enum FF_STATUS_MAX       = 0x01;

/*
 * Structures used in ioctls to upload effects to a device
 * They are pieces of a bigger structure (called ff_effect)
 */

/*
 * All duration values are expressed in ms. Values above 32767 ms (0x7fff)
 * should not be used and have unspecified results.
 */

/**
 * struct ff_replay - defines scheduling of the force-feedback effect
 * @length: duration of the effect
 * @delay: delay before effect should start playing
 */
struct ff_replay {
    ushort length;
    ushort delay;
};

/**
 * struct ff_trigger - defines what triggers the force-feedback effect
 * @button: number of the button triggering the effect
 * @interval: controls how soon the effect can be re-triggered
 */
struct ff_trigger {
    ushort button;
    ushort interval;
};

/**
 * struct ff_envelope - generic force-feedback effect envelope
 * @attack_length: duration of the attack (ms)
 * @attack_level: level at the beginning of the attack
 * @fade_length: duration of fade (ms)
 * @fade_level: level at the end of fade
 *
 * The @attack_level and @fade_level are absolute values; when applying
 * envelope force-feedback core will convert to positive/negative
 * value based on polarity of the default level of the effect.
 * Valid range for the attack and fade levels is 0x0000 - 0x7fff
 */
struct ff_envelope {
    ushort attack_length;
    ushort attack_level;
    ushort fade_length;
    ushort fade_level;
};

/**
 * struct ff_constant_effect - defines parameters of a constant force-feedback effect
 * @level: strength of the effect; may be negative
 * @envelope: envelope data
 */
struct ff_constant_effect {
    short level;
    ff_envelope envelope;
};

/**
 * struct ff_ramp_effect - defines parameters of a ramp force-feedback effect
 * @start_level: beginning strength of the effect; may be negative
 * @end_level: final strength of the effect; may be negative
 * @envelope: envelope data
 */
struct ff_ramp_effect {
    short start_level;
    short end_level;
    ff_envelope envelope;
};

/**
 * struct ff_condition_effect - defines a spring or friction force-feedback effect
 * @right_saturation: maximum level when joystick moved all way to the right
 * @left_saturation: same for the left side
 * @right_coeff: controls how fast the force grows when the joystick moves
 *  to the right
 * @left_coeff: same for the left side
 * @deadband: size of the dead zone, where no force is produced
 * @center: position of the dead zone
 */
struct ff_condition_effect {
    ushort right_saturation;
    ushort left_saturation;

    short right_coeff;
    short left_coeff;

    ushort deadband;
    short center;
};

/**
 * struct ff_periodic_effect - defines parameters of a periodic force-feedback effect
 * @waveform: kind of the effect (wave)
 * @period: period of the wave (ms)
 * @magnitude: peak value
 * @offset: mean value of the wave (roughly)
 * @phase: 'horizontal' shift
 * @envelope: envelope data
 * @custom_len: number of samples (FF_CUSTOM only)
 * @custom_data: buffer of samples (FF_CUSTOM only)
 *
 * Known waveforms - FF_SQUARE, FF_TRIANGLE, FF_SINE, FF_SAW_UP,
 * FF_SAW_DOWN, FF_CUSTOM. The exact syntax FF_CUSTOM is undefined
 * for the time being as no driver supports it yet.
 *
 * Note: the data pointed by custom_data is copied by the driver.
 * You can therefore dispose of the memory after the upload/update.
 */
struct ff_periodic_effect {
    ushort waveform;
    ushort period;
    short magnitude;
    short offset;
    ushort phase;

    ff_envelope envelope;

    uint custom_len;
    short *custom_data;
};

/**
 * struct ff_rumble_effect - defines parameters of a periodic force-feedback effect
 * @strong_magnitude: magnitude of the heavy motor
 * @weak_magnitude: magnitude of the light one
 *
 * Some rumble pads have two motors of different weight. Strong_magnitude
 * represents the magnitude of the vibration generated by the heavy one.
 */
struct ff_rumble_effect {
    ushort strong_magnitude;
    ushort weak_magnitude;
};

/**
 * struct ff_effect - defines force feedback effect
 * @type: type of the effect (FF_CONSTANT, FF_PERIODIC, FF_RAMP, FF_SPRING,
 *  FF_FRICTION, FF_DAMPER, FF_RUMBLE, FF_INERTIA, or FF_CUSTOM)
 * @id: an unique id assigned to an effect
 * @direction: direction of the effect
 * @trigger: trigger conditions (struct ff_trigger)
 * @replay: scheduling of the effect (struct ff_replay)
 * @u: effect-specific structure (one of ff_constant_effect, ff_ramp_effect,
 *  ff_periodic_effect, ff_condition_effect, ff_rumble_effect) further
 *  defining effect parameters
 *
 * This structure is sent through ioctl from the application to the driver.
 * To create a new effect application should set its @id to -1; the kernel
 * will return assigned @id which can later be used to update or delete
 * this effect.
 *
 * Direction of the effect is encoded as follows:
 *  0 deg -> 0x0000 (down)
 *  90 deg -> 0x4000 (left)
 *  180 deg -> 0x8000 (up)
 *  270 deg -> 0xC000 (right)
 */
struct ff_effect {
    ushort type;
    short id;
    ushort direction;
    ff_trigger trigger;
    ff_replay replay;

    union U {
        ff_constant_effect constant;
        ff_ramp_effect ramp;
        ff_periodic_effect periodic;
        ff_condition_effect[2] condition; /* One for each axis */
        ff_rumble_effect rumble;
    }
	U u;
};

/*
 * Force feedback effect types
 */

enum FF_RUMBLE   = 0x50;
enum FF_PERIODIC = 0x51;
enum FF_CONSTANT = 0x52;
enum FF_SPRING   = 0x53;
enum FF_FRICTION = 0x54;
enum FF_DAMPER   = 0x55;
enum FF_INERTIA  = 0x56;
enum FF_RAMP     = 0x57;

enum FF_EFFECT_MIN   = FF_RUMBLE;
enum FF_EFFECT_MAX   = FF_RAMP;

/*
 * Force feedback periodic effect types
 */

enum FF_SQUARE   = 0x58;
enum FF_TRIANGLE = 0x59;
enum FF_SINE     = 0x5a;
enum FF_SAW_UP   = 0x5b;
enum FF_SAW_DOWN = 0x5c;
enum FF_CUSTOM   = 0x5d;

enum FF_WAVEFORM_MIN = FF_SQUARE;
enum FF_WAVEFORM_MAX = FF_CUSTOM;

/*
 * Set ff device properties
 */

enum FF_GAIN     = 0x60;
enum FF_AUTOCENTER   = 0x61;

/*
 * ff->playback(effect_id = FF_GAIN) is the first effect_id to
 * cause a collision with another ff method, in this case ff->set_gain().
 * Therefore the greatest safe value for effect_id is FF_GAIN - 1,
 * and thus the total number of effects should never exceed FF_GAIN.
 */
enum FF_MAX_EFFECTS  = FF_GAIN;

enum FF_MAX      = 0x7f;
enum FF_CNT      = (FF_MAX+1);
