import std.stdio;
import std.string;

import core.sys.posix.sys.time;
import core.thread;

import linux.input;
import linux.input_event_codes;

import screen;
import uinput;

class Action
{
	static __gshared Action instance;

	bool active = true;

	this()
	{
		if (instance !is null)
			throw new Exception("Another action in progress!");
		instance = this;
		new Thread(&run).start();
	}

	private final void run()
	{
		scope(exit) instance = null;
		try
			impl();
		catch (Throwable e)
		{
			stderr.writeln(e);
			throw e;
		}
	}

	protected abstract void impl();
}

class InputAction : Action
{
	private @property UInputWriter writer()
	{
		static __gshared UInputWriter writer;
		synchronized(typeof(this).classinfo)
		{
			if (writer is null)
				writer = new UInputWriter(this.classinfo.name.toStringz);
			return writer;
		}
	}

	this()
	{
	}

	void send(ushort type, ushort code, int value = 0)
	{
		auto ev = input_event(timeval.init, type, code, value);
		writer.send(ev);
	}

	void flush()
	{
		send(EV_SYN, SYN_REPORT);
	}

	enum frameDuration = 1.seconds / 60;
	enum syncTimeout = 150.msecs;
	enum doubleClickTimeout = 325.msecs; // ±25ms

	void rawMouseMoveRel(int x, int y)
	{
		send(EV_REL, REL_X, x);
		send(EV_REL, REL_Y, y);
		flush();
	}

	void mouseMove(int x, int y)
	{
		int mx, my;
		import std.stdio; stderr.writeln("Finding mouse...");
		while (!findMouse(mx, my))
			sleep(frameDuration);

		int tries;
		while (++tries < 10)
		{
			if (mx == x && my == y)
				return;

			import std.stdio; stderr.writeln("Moving mouse...");
			rawMouseMoveRel(x-mx, y-my);

			// Wait for the mouse to move (with a timeout)
			int mx2, my2;
			auto start = MonoTime.currTime;
			while (true)
			{
				while (!findMouse(mx2, my2))
					sleep(frameDuration);
				if (mx != mx2 || my != my2)
					break;
				if (MonoTime.currTime - start >= syncTimeout)
					break;
				import std.stdio; stderr.writeln("Waiting for mouse to move...");
				sleep(frameDuration);
			}
			mx = mx2; my = my2;
		}
		import std.stdio; stderr.writefln("Failed to move the mouse after %d tries", tries);
		throw new Exception("mouseMove timeout");
	}

	enum Button { left, right }

	void click(Button button = Button.left)
	{
		send(EV_KEY, cast(ushort)(BTN_LEFT + button), 1); flush();
		send(EV_KEY, cast(ushort)(BTN_LEFT + button), 0); flush();
	}

	void click(int x, int y, Button button = Button.left)
	{
		mouseMove(x, y);
		click(button);
	}
}
