import core.thread;

import std.algorithm.iteration;
import std.array;
import std.stdio;
import std.typecons;

import ae.sys.sendinput;
import ae.utils.geometry;
import ae.utils.graphics.color;
import ae.utils.graphics.image;

enum screenW = 360;
enum screenH = 240;

enum screenX0 = 200;
enum screenY0 =   0;
enum screenPW =   6;
enum screenPH =   6;

RGB gamePixel(int x, int y)
{
	assert(x >= 0 && x < 360 && y >= 0 && y < 240);
	auto p = getPixel(
		screenX0 + screenPW * x + screenPW / 2,
		screenY0 + screenPH * y + screenPH / 2,
	);
	return RGB(p.r, p.g, p.b);
}

bool isGameRunning()
{
	return gamePixel(  0, 192) == RGB(0x86, 0x8A, 0x8A);
}

bool isInMap()
{
	return gamePixel(  8,  80) == RGB(0x34, 0x3C, 0x51);
}

bool isIn3DMode()
{
	return gamePixel( 21,  19) == RGB(0x1C, 0x45, 0x24);
}

bool isInBattle()
{
	return gamePixel(138, 155) == RGB(0x71, 0x6D, 0x69);
}

bool isInQuery()
{
	return gamePixel( 90, 103) == RGB(0xE7, 0xC3, 0x30);
}

bool isMouseAt(int x, int y)
{
	import core.stdc.stdlib; exit(0);
	auto p0 = gamePixel(x  , y);
	auto p1 = gamePixel(x+1, y);
	auto p2 = gamePixel(x+2, y);
	return p0 ==
		RGB(0xFF, 0xFF, 0xFF)
		&&
		(
			(p1 == RGB(0xC3, 0xBE, 0xBA) && p2 == RGB(0x75, 0x7D, 0x8E))
			||
			(p2 == RGB(0xFF, 0xE3, 0x71) && p2 == RGB(0xE7, 0xC3, 0x30))
		);
}

void sleep(Duration duration)
{
	Thread.sleep(duration);
	screenDirty = true;
}

bool screenDirty = true;
Image!RGB screenBuffer; // use getScreen, not this

ref Image!RGB getScreen()
{
	if (screenDirty)
	{
		writeln("Updating screen...");
		screenBuffer.size(screenW, screenH);
		foreach (y; 0..screenH)
		{
			static Image!BGRA r;
			captureRect(Rect!int(
					screenPW / 2 + screenX0,
					screenPH / 2 + screenY0 + y * screenPH,
					screenPW / 2 + screenX0 + screenW * screenPW,
					screenPH / 2 + screenY0 + y * screenPH + 1), r);
			foreach (x; 0..screenW)
				screenBuffer[x, y] = r.colorMap!(p => RGB(p.r, p.g, p.b))[x * screenPW, 0];
		}

		screenDirty = false;
	}

	return screenBuffer;
}

immutable Image!RGB[] cursors;

immutable(Image!RGB) loadSprite(string fn)
{
	import std.file : read;
	return cast(immutable)fn.read.parseBMP!BGR.colorMap!(c => RGB(c.r, c.g, c.b)).copy;
}

shared static this()
{
	import std.file : dirEntries, SpanMode, read;
	foreach (de; dirEntries("", "mouse*.bmp", SpanMode.shallow))
		cursors ~= loadSprite(de.name);
}

bool spriteMatch(in ref Image!RGB sprite, int x, int y, in ref Image!RGB screen)
{
	foreach (cy; 0..sprite.h)
		foreach (cx; 0..sprite.w)
		{
			auto cp = sprite[cx, cy];
			if (cp == RGB(0xFF, 0x00, 0xFF))
				continue;
			auto sx = x + cx;
			auto sy = y + cy;
			if (sx < 0 || sy < 0 || sx >= screenW || sy >= screenH)
				continue;
			auto sp = screen[sx, sy];
			if (cp != sp)
				return false;
		}
	return true;
}

Tuple!(int, int, size_t)[] findSprites(in Image!RGB[] sprites, int ox, int oy, int x0 = 0, int y0 = 0, int x1 = screenW, int y1 = screenH)
{
	auto screen = getScreen();
	Tuple!(int, int, size_t)[] results;

	foreach (y; y0..y1)
		foreach (x; x0..x1)
			foreach (ref s, sprite; sprites)
				if (spriteMatch(sprite, x + ox, y + oy, screen))
				{
					results ~= tuple(x, y, s);
					break;
				}
	return results;
}

int findMouse(out int mx, out int my)
{
	auto result = findSprites(cursors, -1, -1);
	if (result.length > 1)
	{
		writefln("Found more than one mouse cursor: %s", result);
		return 0;
	}
	else
	if (result.length == 0)
		return 0;
	else
	{
		mx = result[0][0];
		my = result[0][1];
		return 1 + cast(int)result[0][2];
	}
}

immutable Image!RGB frameCorner;
shared static this() { frameCorner = loadSprite("menu.bmp"); }

Tuple!(int, int)[] findFrames()
{
	return findSprites((&frameCorner)[0..1], 3, 3, 0, 0, screenW - 10, screenH - 10).map!(t => tuple(t[0], t[1])).array;
}

