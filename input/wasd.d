import core.stdc.stdio;
import core.sys.posix.sys.time;
import core.thread;

import uinput;
import action;
import combat;
import screen;

import linux.uinput;
import linux.input;
import linux.input_event_codes;

class InputStateFilter : UInputFilter
{
	this(const char* upstreamDevice)
	{
		super(upstreamDevice);
	}

	alias KeyState = bool[KEY_CNT];
	KeyState inputState, lastOutputState;
	bool dirty;

	final override void processEvent(ref input_event ev)
	{
		//printf("Got event! type=%d code=%d value=%d\n", ev.type, ev.code, ev.value);

		if (ev.type == EV_KEY)
			inputState[ev.code] = ev.value != 0;
		else
		if (ev.type == EV_SYN && ev.code == SYN_REPORT)
			update(ev.time);
		else
		{
			send(ev);
			dirty = true;
		}
	}

	final void update(in ref timeval time)
	{
		auto outputState = inputState;
		translateKeys(outputState);

		foreach (ushort key; 0..KEY_CNT)
			if (outputState[key] != lastOutputState[key])
			{
				auto ev = input_event(time, EV_KEY, key, outputState[key]);
				send(ev);
				dirty = true;
			}
		lastOutputState = outputState;

		if (dirty)
		{
			auto ev = input_event(time, EV_SYN, SYN_REPORT, 0);
			send(ev);
			dirty = false;
		}
	}

	protected void translateKeys(ref KeyState state)
	{
	}
}

class AlbionFilter : InputStateFilter
{
	this()
	{
		super("/dev/input/by-id/usb-TrulyErgonomic.com_Truly_Ergonomic_Computer_Keyboard-event-kbd");
	}

	bool active, wasdActive, wasdStrafe;
	bool doCombat;

	override void translateKeys(ref KeyState state)
	{
		void translate(ushort from, ushort[] to...)
		{
			if (state[from])
			{
				state[from] = false;
				foreach (toKey; to)
					state[toKey] = true;
			}
		}

		if (wasdActive)
		{
			translate(KEY_W, KEY_UP);
			translate(KEY_S, KEY_DOWN);

			if (wasdStrafe)
			{
				translate(KEY_Q, KEY_LEFT);
				translate(KEY_E, KEY_RIGHT);
				translate(KEY_A, KEY_RIGHTALT, KEY_LEFT);
				translate(KEY_D, KEY_RIGHTALT, KEY_RIGHT);
			}
			else
			{
				translate(KEY_A, KEY_LEFT);
				translate(KEY_D, KEY_RIGHT);
			}
		}

		if (active && !wasdStrafe)
			translate(KEY_LEFTCTRL, KEY_LEFTALT, KEY_F12);

		bool checkKey(short key)
		{
			if (active && state[key])
			{
				state[key] = false;
				return true;
			}
			else
				return false;
		}

		doCombat = checkKey(KEY_C);
	}
}

void main()
{
	import std.stdio;

	auto f = new AlbionFilter();

	new Thread({
		try
		while (true)
		{
			bool gameRunning = isGameRunning();
			bool inMap = gameRunning && isInMap();
			bool in3DMode = inMap && isIn3DMode();
			inMap = inMap && (in3DMode || !isInQuery());
			f.active = gameRunning;
			f.wasdActive = inMap;
			f.wasdStrafe = in3DMode;

			void checkAction(T)(bool active)
			{
				if (!Action.instance)
				{
					if (active)
						new T();
				}
				else
				if (cast(T)Action.instance)
					Action.instance.active = active;
			}

			checkAction!CombatAction(f.doCombat);

			sleep(10.msecs);
		}
		catch (Throwable e)
			writeln(e);
	}).start();

	stderr.writeln("Running.");
	while (true)
		try
		{
			if (!f)
				f = new AlbionFilter();
			f.run();
		}
		catch (Exception e)
		{
			writeln(e);
			Thread.sleep(1.seconds);
			f.destroy();
			f = null;
		}
}
