import std.exception;

import core.stdc.stdio;
import core.stdc.string;
import core.sys.posix.fcntl;
import core.sys.posix.unistd;
import core.sys.posix.sys.ioctl;

import linux.uinput;
import linux.input;
import linux.input_event_codes;

class UInputWriter
{
	int fdo;

	this(const char *name)
	{
		fdo = open("/dev/uinput", O_WRONLY | O_NONBLOCK);
		errnoEnforce(fdo >= 0, "open");

		errnoEnforce(ioctl(fdo, UI_SET_EVBIT, EV_SYN) >= 0, "error: ioctl");
		errnoEnforce(ioctl(fdo, UI_SET_EVBIT, EV_KEY) >= 0, "error: ioctl");
		errnoEnforce(ioctl(fdo, UI_SET_EVBIT, EV_REL) >= 0, "error: ioctl");
		errnoEnforce(ioctl(fdo, UI_SET_EVBIT, EV_MSC) >= 0, "error: ioctl");

		foreach (i; 0..KEY_MAX)
			errnoEnforce(ioctl(fdo, UI_SET_KEYBIT, i) >= 0, "error: ioctl");
		foreach (i; 0..REL_MAX)
			errnoEnforce(ioctl(fdo, UI_SET_RELBIT, i) >= 0, "error: ioctl");

		uinput_user_dev uidev;
		memset(&uidev, 0, uidev.sizeof);
		snprintf(uidev.name.ptr, UINPUT_MAX_NAME_SIZE, name);
		uidev.id.bustype = BUS_USB;
		uidev.id.vendor  = 0x1;
		uidev.id.product = 0x1;
		uidev.id.version_ = 1;

		errnoEnforce(write(fdo, &uidev, uidev.sizeof) >= 0, "error: write");
		errnoEnforce(ioctl(fdo, UI_DEV_CREATE) >= 0, "error: ioctl");
	}

	final void send(in ref input_event ev)
	{
		enforce(write(fdo, &ev, input_event.sizeof) >= 0, "error: write");
	}

	~this()
	{
		errnoEnforce(ioctl(fdo, UI_DEV_DESTROY) >= 0, "error: ioctl");
		close(fdo);
	}
}

class UInputFilter
{
	int fdi;
	UInputWriter writer;

	this(const char* upstreamDevice)
	{
		writer = new UInputWriter("UInputFilter");

		fdi = open(upstreamDevice, O_RDONLY);
		errnoEnforce(fdi >= 0, "open");

		errnoEnforce(ioctl(fdi, EVIOCGRAB, 1) >= 0, "error: ioctl");
	}

	final void run()
	{
		while (true)
		{
			input_event ev;
			enforce(read(fdi, &ev, input_event.sizeof) >= 0, "error: read");

			processEvent(ev);
		}
	}

	final void send(in ref input_event ev)
	{
		writer.send(ev);
	}

	/// Override this
	void processEvent(ref input_event ev)
	{
		send(ev);
	}

	~this()
	{
		close(fdi);
	}
}

/// Example
class UInputFilterMouse : UInputFilter
{
	this()
	{
		super("/dev/input/by-id/usb-Logitech_G700_Laser_Mouse_37ADAF750037-event-mouse");
	}

	override void processEvent(ref input_event ev)
	{
		if (ev.type == EV_KEY)
		{
			int key = ev.code;
			if (key == BTN_EXTRA || key == BTN_SIDE)
			{
				ev.code = KEY_LEFTMETA;
				send(ev);

				if (key == BTN_EXTRA)
					ev.code = BTN_LEFT;
				if (key == BTN_SIDE)
					ev.code = BTN_RIGHT;
				send(ev);
			}
		}

		send(ev);
	}
}
